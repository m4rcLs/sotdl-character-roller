import { createApp } from 'vue'
import { createPinia } from 'pinia';
import App from './App.vue'

import './../node_modules/bulma/css/bulma.css';
import './../node_modules/bulma-tooltip/dist/css/bulma-tooltip.min.css';
import './../node_modules/fontawesome-free/css/all.min.css';

createApp(App).use(createPinia()).mount('#app')

