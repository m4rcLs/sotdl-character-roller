import { defineStore } from "pinia";
import { ancestries } from '../assets/data.json';

export const useAncestryStore = defineStore("ancestries", {
    state: () => {
        return {
            ancestries: ancestries.map((ancestry, index) => { return { ...ancestry, id: index } })
        }
    },
})