import { defineStore } from "pinia";
import { generateCharacters } from "../components/generate";
import { Character, GenerationOptions } from "../components/models";

const STORAGE_KEY = "savedCharacters"
function loadCharacters(): Character[] {
    const characters: Character[] = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]')
    return characters;
}

export const useCharacterStore = defineStore("characters", {
    state: () => {
        return {
            characters: [] as Character[], options: { includeMagical: true, includeMonstrous: true } as GenerationOptions,
            savedCharacters: loadCharacters(),
        }
    },

    actions: {
        generateNewCharacters() {
            this.characters = generateCharacters(3, this.options);
        },

        loadCharacters() { this.savedCharacters = loadCharacters() },

        viewSavedCharacters() { this.characters = this.savedCharacters },

        saveCharacter(character: Character) {
            character.id = this.savedCharacters.length + 1;
            this.savedCharacters = [...this.savedCharacters, character];
            this.saveCharacters(this.savedCharacters);
        },

        saveCharacters(characters: Character[]) {
            localStorage.setItem(STORAGE_KEY, JSON.stringify(characters));
        },

        deleteCharacter(id: number) {
            this.savedCharacters = this.savedCharacters.filter(character => character.id !== id);
            this.saveCharacters(this.savedCharacters);
        }
    }
})