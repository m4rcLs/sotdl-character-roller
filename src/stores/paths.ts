import { defineStore } from "pinia";
import { novice, expert, master } from '../assets/data.json';
// const ANCESTRIES = ancestries.map((ancestry, index) => { return { ...ancestry, id: index } })

export const usePathStore = defineStore("paths", {
    state: () => {
        return {
            novice: novice.map((path, index) => { return { ...path, id: index } }),
            expert: expert.map((path, index) => { return { ...path, id: index } }),
            master: master.map((path, index) => { return { ...path, id: index } })
        }
    },
})