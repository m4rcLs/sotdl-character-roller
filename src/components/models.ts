export type Path = {
    id?: number;
    name: string;
    description: string;
    magical?: boolean;
}

export type Ancestry = {
    id?: number;
    name: string;
    description: string;
    monstrous?: boolean;
}

export type Character = {
    id?: number;
    ancestryId?: number;
    novicePathId?: number;
    expertPathId?: number;
    masterPathId?: number;
}

export type GenerationOptions = {
    includeMagical: boolean;
    includeMonstrous: boolean;
}