import { useAncestryStore } from '../stores/ancestries';
import { usePathStore } from '../stores/paths';
import { Character, GenerationOptions, Path } from './models';

function getMartialPaths(paths: Path[]): Path[] {
    return paths.filter((path) => !path.magical)
}

function filterPaths(paths: Path[], includeMagical: boolean = true): Path[] {
    return includeMagical ? paths : getMartialPaths(paths)
}

function getRandomElement(array: any[]): any {
    return array[
        Math.floor(Math.random() * array.length)
    ]
}

export function generateCharacter(options: GenerationOptions): Character {
    const { ancestries } = useAncestryStore();
    const { novice, expert, master } = usePathStore();
    let filteredAncestries = ancestries.filter((ancestry) => options.includeMonstrous || !ancestry.monstrous)
    let filteredNovices = filterPaths(novice, options.includeMagical);
    let filteredExpert = filterPaths(expert, options.includeMagical);
    let filteredMaster = filterPaths(master, options.includeMagical);

    const ancestry = getRandomElement(filteredAncestries)
    const novicePath = getRandomElement(filteredNovices)
    const expertPath = getRandomElement(filteredExpert)
    const masterPath = getRandomElement(filteredMaster)
    const character: Character = {
        ancestryId: ancestry.id,
        novicePathId: novicePath.id,
        expertPathId: expertPath.id,
        masterPathId: masterPath.id,
    };
    return character;
}

export function generateCharacters(count: number, options: GenerationOptions): Character[] {
    const characters: Character[] = [];

    for (let i = 0; i < count; ++i) {
        characters.push(generateCharacter(options));
    };

    return characters;
}